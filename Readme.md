# Project Setup
1. Install Eclipse (4.5/Mars or later)
2. Install the Eclipse plug-in development environment
    1. Go to Help -> Install New Software ...
    2. Select `--All Available Sites--` in the upper dropdown menu
    3. Search and install `Eclipse plug-in development environment`
    4. Follow the installation process
3. Install Scala IDE for Eclipse
    1. Go to Help -> Install New Software ...
    2. Enter `http://download.scala-ide.org/sdk/lithium/e44/scala211/stable/site` where you can select an update site
    3. Search and install `Scala IDE for Eclipse`
    4. Follow the installation process
4. Import the Eclipse OPAL Project  
    Clone with your favorite git program and use the import dialog or use the eclipse git integration