/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2015
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.opaleclipse.actions

import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.IWorkbenchPage
import org.eclipse.ui.IWorkbenchPart
import org.eclipse.ui.handlers.HandlerUtil
import org.opalj.opaleclipse.views.DisassemblerView

/**
 * @author Lukas Becker
 * @author Simon Bohlender
 * @author Simon Guendling
 * @author Felix Zoeller
 */
class DisassemblerActionHandler extends AbstractHandler {

    @Override
    @throws(classOf[ExecutionException])
    def execute(event: ExecutionEvent): Object = {
        try {
            val (jarPath: String, classFileName: String) = extractLocation(event)
            val activePage: IWorkbenchPage = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage()
            val view: IWorkbenchPart = activePage.showView("org.opalj.opaleclipse.views.DisassemblerView")
            view match {
                case disassemblerView: DisassemblerView ⇒
                    disassemblerView.showDisassembly(jarPath, classFileName)
            }
        } catch {
            case e: IllegalArgumentException ⇒ MessageDialog.openInformation(HandlerUtil.getActiveWorkbenchWindow(event).getShell(), "Error", "Bytecode Disassembler is only available for .class files")
        }
        null
    }

    /**
     * @return A tuple containing the path to the selected .jar and .class file.
     */
    @throws(classOf[IllegalArgumentException])
    def extractLocation(event: ExecutionEvent): (String, String) = {
        HandlerUtil.getCurrentSelection(event) match {
            case s: IStructuredSelection ⇒
                s.getFirstElement match {
                    case ije: IJavaElement ⇒
                        val jarPath = ije.getPath.toOSString()
                        val packageName = ije.getParent.toString.split(" ")(0).replaceAll("\\.", "/")+"/"
                        val classFileName = packageName + ije.getElementName
                        (jarPath, classFileName)
                    case _ ⇒
                        throw new IllegalArgumentException("Invalid JarFile")
                }
            case _ ⇒
                throw new IllegalArgumentException("Invalid ClassFile")
        }
    }
}